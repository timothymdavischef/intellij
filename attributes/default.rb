default["intellij"]["edition"] = "ideaIU" # Only ideaIU and ideaIC are supported.
default["intellij"]["version"] = "12.1.6"
default["intellij"]["build"] = "129.1359"
default["intellij"]["key"] = "" # While you can set this here, you might consider overridding it elsewhere.
