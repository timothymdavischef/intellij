#
# Cookbook Name:: intellij
# Recipe:: default
#
# Copyright 2014, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
#

unless node["intellij"]["edition"].include? "ideaIU" or node["intellij"]["edition"].include? "ideaIC"
  raise "The edition \"#{node["intellij"]["edition"]}\" is invalid. Only \"ideaIU\" or \"ideaIC\" are supported."
end

download_file_name = "#{node["intellij"]["edition"]}-#{node["intellij"]["version"]}.tar.gz"
download_location = "#{Chef::Config[:file_cache_path]}/#{download_file_name}"

directory "/usr/local/share/applications" do
  mode 00755
  recursive true
end

remote_file "#{download_location}" do
  source "http://download.jetbrains.com/idea/#{download_file_name}"
  mode "0644"
  notifies :run, "execute[untar-intellij]", :immediately
end

execute "untar-intellij" do
  command "tar -xzf #{download_location} -C /usr/local/share/"
  action :nothing
end

template "/usr/local/share/applications/idea.desktop" do
  source "idea.desktop.erb"
  variables({
     :name_and_build => "#{node["intellij"]["edition"][0..3]}-#{node["intellij"]["edition"][4..5]}-#{node["intellij"]["build"]}"
  })
end
